import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

import { AlertController } from "ionic-angular";


@Injectable()
export class AuthService {

  user: Observable<firebase.User>;

  constructor(
    public alertCtrl: AlertController,
    public auth: AngularFireAuth,
    public http: Http) {
    //console.log('Hello AuthProvider Provider');
    this.user = auth.authState;
  }
  createUser(user: { email: string, password: string }) {
    this.auth.
      auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(value => {
        //console.log('Usuário cadastrado com Sucesso', value);
      })
      .catch(err => {
        console.log('Algum erro aconteceu. Informe o código do erro para o pessoal do TI:', err.message);
      });
  }

  loginComEmail(user: { email: string, password: string }) {
    return this.auth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then((authState: AngularFireAuth) => {
        return authState != null;
      }).catch();
  }

  logout(): Promise<void> {
    return this.auth.auth.signOut();
  }

  get authenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.auth
        .authState.
        subscribe((authState) => {
          (authState) ? resolve(true) : reject(false);
        });
    });
  }

}
