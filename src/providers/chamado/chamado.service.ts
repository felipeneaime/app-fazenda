import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { Chamado } from "../../models/chamado.model";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ChamadoService {

  api: string = 'https://cnnovelty.000webhostapp.com/';

  constructor(
    public db: AngularFireDatabase,
    public http: Http) {
    console.log('Hello ChamadoProvider Provider');
  }

  getData(req) {
    return this.http.get(this.api + 'apiRecupera.php?email='+req).map(res=>res.json())
  }

  editChamado(parans) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(this.api + "apiUpdate.php", parans, {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => { return res.json(); }
      );
  }

  postChamado(parans) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(this.api + "apiCadastro.php", parans, {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => { return res.json(); }
      );
  }

  createChamado(chamado: Chamado) {
    return this.db.list('/chamados').push(chamado);
  }

}
