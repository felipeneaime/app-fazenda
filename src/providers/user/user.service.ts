import { Http, Headers, Response, ResponseOptions  } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


import { AngularFireModule } from "angularfire2";
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { User } from "../../models/user.model";
import { Item } from "ionic-angular";



@Injectable()
export class UserService {

  //users: AngularFireList<User[]>
  //items: Observable<any[]>;
  
  api: string = 'https://cnnovelty.000webhostapp.com/';

  constructor(
    public db: AngularFireDatabase,
    public af: AngularFireAuth,
    public http: Http) {
    //const users = db.list('/users', ref => ref.orderByChild('name'));
    //this.users = this.db.list('/users', ref => ref.orderByChild('cargo'));
    //db.list('/users', ref => ref.orderByChild('setor'));
    //this.users = this.db.list('/users');
    //console.log((this.users));
  }
  create(user: User){
    return this.db.list('/users').push(user);
  }

  createUserSQL(parans) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(this.api + "apiCadastroUser.php", parans, {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => { return res.json(); }
      );
  }

}
