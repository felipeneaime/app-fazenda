import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { AddchamadoPage } from "../pages/addchamado/addchamado";
import { EditchamadoPage } from "../pages/editchamado/editchamado";
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from './../pages/login/login';
import { RegistrarPage } from './../pages/registrar/registrar';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { FirebaseAppConfig, AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";
import { UserService } from "../providers/user/user.service";
import { AuthService } from "../providers/auth/auth.service";

import { ChamadoService } from '../providers/chamado/chamado.service';

const firebaseAppConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyCKYqjJc4qfgoM0CYmSNzm3pEwM7vSVtes",
  authDomain: "fazendagrama-48a51.firebaseapp.com",
  databaseURL: "https://fazendagrama-48a51.firebaseio.com",
  projectId: "fazendagrama-48a51",
  storageBucket: "fazendagrama-48a51.appspot.com",
  messagingSenderId: "412609420557"
}; 

@NgModule({
  declarations: [
    MyApp,
    AddchamadoPage,
    EditchamadoPage,
    HomePage,
    ListPage,
    LoginPage,
    RegistrarPage,
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    HttpModule,
    HttpClientModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAppConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddchamadoPage,
    EditchamadoPage,
    HomePage,
    ListPage,
    LoginPage,
    RegistrarPage,
  ],
  providers: [
    AuthService,
    UserService,
    ChamadoService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
