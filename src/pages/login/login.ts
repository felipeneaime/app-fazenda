import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { UserService } from './../../providers/user/user.service';
import { Component } from '@angular/core';
import { NavController, NavParams, Item, Loading, LoadingController, AlertController } from 'ionic-angular';

import { RegistrarPage } from './../registrar/registrar';
import { HomePage } from './../home/home';


import { AuthService } from './../../providers/auth/auth.service';
import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { User } from "../../models/user.model";
import { Observable } from "rxjs/Observable";

//import { User } from './../../models/user';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;

  //users: AngularFireList<User[]>;

  users: Observable<any[]>;
  
  constructor(
    public authService: AuthService,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public userService: UserService,
    public db: AngularFireDatabase) {

      let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      
            this.loginForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
            password: ['', [Validators.required, Validators.minLength(6)]],
            });

      //this.users = db.list('/users', ref => ref.orderByChild('name')).valueChanges();
  }

  onSubmit(): void{
    //console.log(this.loginForm.value);
    let loading: Loading = this.showLoading();
    this.authService.loginComEmail(this.loginForm.value)
    .then((isLogged: boolean) => {
      if(isLogged){
        this.navCtrl.setRoot(HomePage);
        loading.dismiss();
      }
    }).catch((erro: any) => {
      console.log(erro);
      loading.dismiss();
      this.showAlert(erro);
    });
  }

  onRegistrar(){
    this.navCtrl.push(RegistrarPage)
  }

  onHomePage(){
    this.navCtrl.push(HomePage)
  }

  onLogout(){
    this.authService.logout();
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor Aguarde...'
    });
    loading.present();
    return loading;
  }

  private showAlert(message): void{
    this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    }).present();
  }

}
