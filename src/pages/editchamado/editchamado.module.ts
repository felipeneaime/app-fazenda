import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditchamadoPage } from './editchamado';

@NgModule({
  declarations: [
    EditchamadoPage,
  ],
  imports: [
    IonicPageModule.forChild(EditchamadoPage),
  ],
})
export class EditchamadoPageModule {}
