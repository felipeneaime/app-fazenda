import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';

import { ChamadoService } from "../../providers/chamado/chamado.service";
import { HomePage } from './../home/home';

import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

/**
 * Generated class for the EditchamadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editchamado',
  templateUrl: 'editchamado.html',
})
export class EditchamadoPage {
  descricao: any;
  prioridade: any;
  categoria: any;
  titulo: any;

  editchamadoForm: FormGroup;
  email: string;
  id;
  listChamados: any[];

  constructor(
    public chamadoService: ChamadoService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams) {
    
      let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.editchamadoForm = this.formBuilder.group({
      id: ['', [Validators.required, Validators.minLength(1)]],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      categoria: ['', [Validators.required, Validators.minLength(2)]],
      prioridade: ['', [Validators.required, Validators.minLength(1)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
    });

    var user = firebase.auth().currentUser;
    this.email = user.email;
  }

  ionViewDidLoad(navParams: NavParams) {
    this.id = this.navParams.get("id");
    this.titulo = this.navParams.get("titulo");
    this.categoria = this.navParams.get("categoria");
    this.prioridade = this.navParams.get("prioridade");
    this.descricao = this.navParams.get("descricao");
    this.email = this.navParams.get("email");
  }

  editChamado(): void{
    let loading: Loading = this.showLoading();
    console.log(this.editchamadoForm.value);
    this.chamadoService.editChamado(this.editchamadoForm.value)
    .subscribe(
    data => console.log(data.mensage),
    //err=>console.log(err)
    );
    loading.dismiss();
    this.showAlert();
    this.navCtrl.setRoot(HomePage);
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor Aguarde...'
    });
    loading.present();
    return loading;
  }

  private showAlert(): void {
    this.alertCtrl.create({
      message: 'Chamado Atualizado',
      buttons: ['OK']
    }).present();
  }
}
