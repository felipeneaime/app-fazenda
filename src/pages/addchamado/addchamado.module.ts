import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddchamadoPage } from './addchamado';

@NgModule({
  declarations: [
    AddchamadoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddchamadoPage),
  ],
})
export class AddchamadoPageModule {}
