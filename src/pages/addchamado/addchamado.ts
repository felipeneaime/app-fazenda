import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ChamadoService } from "../../providers/chamado/chamado.service";
import { HomePage } from "../home/home";

import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-addchamado',
  templateUrl: 'addchamado.html',
})
export class AddchamadoPage {

  chamadoForm: FormGroup;
  email: string;

  constructor(
    public chamadoService: ChamadoService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams) {

    let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.chamadoForm = this.formBuilder.group({
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      categoria: ['', [Validators.required, Validators.minLength(2)]],
      prioridade: ['', [Validators.required, Validators.minLength(1)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
      status: ['', [Validators.required, Validators.minLength(3)]],
    });

    var user = firebase.auth().currentUser;
    this.email = user.email;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddchamadoPage');
  }


  onChamado(req): void {

    let loading: Loading = this.showLoading();
    let chamado: ChamadoService = this.chamadoForm.value;


    //console.log(this.registrarForm.value);
    this.chamadoService.createChamado(this.chamadoForm.value);
    loading.dismiss();
    this.showAlert();

    this.chamadoService.postChamado(this.chamadoForm.value)
      .subscribe(
      data => console.log(data.mensage),
      //err=>console.log(err)
    );

    this.navCtrl.setRoot(HomePage);

  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor Aguarde...'
    });
    loading.present();
    return loading;
  }

  private showAlert(): void {
    this.alertCtrl.create({
      message: 'Chamado Registrado',
      buttons: ['OK']
    }).present();
  }

}
