import { Component } from '@angular/core';
import { NavController, Item, Loading, LoadingController } from 'ionic-angular';

import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { User } from "../../models/user.model";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/filter';

import { AuthService } from "../../providers/auth/auth.service";
import { UserService } from "../../providers/user/user.service";
import { ChamadoService } from '../../providers/chamado/chamado.service';

import { AddchamadoPage } from "../addchamado/addchamado";
import { EditchamadoPage } from "../editchamado/editchamado";
import { LoginPage } from "../login/login";

import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  listChamados: any[];

  email: string;
  users: Observable<any[]>;
  chamados: Observable<any[]>;



  constructor(
    public authService: AuthService,
    public chamadoService: ChamadoService,
    public db: AngularFireDatabase,
    public navCtrl: NavController,
    public userService: UserService,
    public loadingCtrl: LoadingController,
  ) {

    var user = firebase.auth().currentUser;
    if (user != null) {
      this.email = user.email;
      /*
      var aux = user.email;
      var refs = firebase.database().ref("/chamados");
      refs.orderByChild('email').equalTo(aux).once('value').then(function (snapshot) {

        snapshot.forEach(function (childSnapshot) {

          var value = childSnapshot.val();
          console.log("Title is : " + value.titulo);
          return value;
        });
        //console.log(snapshot.val().email);
        //console.log(snapshot.key); // 'users'
        //console.log(snapshot.child('email').key)
      });
      */
    } else {
      console.log("nenhum usuario logado!")
    }

    this.chamados = db.list('/chamados', ).valueChanges();
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.authService.authenticated;
  }

  ionViewDidLoad(req) {

    let loading: Loading = this.showLoading();
        
    var user = firebase.auth().currentUser;
    this.email = user.email;

      //retorno de Dados
      this.chamadoService.getData(user.email)
            .subscribe(
                  data=> this.listChamados = data,
                  err=> console.log(err)
            );
    loading.dismiss();
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor Aguarde...'
    });
    loading.present();
    return loading;
  }


  onLogout() {
    this.authService.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  editChamado(listChamadoid, listChamadotitulo, listChamadocategoria, listChamadoprioridade, listChamadodescricao){
    var user = firebase.auth().currentUser;
    this.email = user.email;
    this.navCtrl.push(EditchamadoPage, {id: listChamadoid, titulo: listChamadotitulo, categoria: listChamadocategoria, prioridade: listChamadoprioridade, descricao: listChamadodescricao, email: user.email});
    console.log(user.email);
  }

  addChamado() {
    this.navCtrl.push(AddchamadoPage);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
}
