import { HomePage } from './../home/home';
import { UserService } from './../../providers/user/user.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


import { User } from "../../models/user.model";
import { AuthService } from "../../providers/auth/auth.service";

@Component({
  selector: 'page-registrar',
  templateUrl: 'registrar.html',
})
export class RegistrarPage {
  
  registrarForm: FormGroup;
  
    constructor(
      public authService: AuthService,
      public formBuilder: FormBuilder,
      public loadingCtrl: LoadingController,
      public alertCtrl: AlertController,
      public navCtrl: NavController,
      public navParams: NavParams,
      public userService: UserService) {
  
        let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  
        this.registrarForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        cargo: ['', [Validators.required, Validators.minLength(3)]],
        setor: ['', [Validators.required, Validators.minLength(2)]],
        email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        password: ['', [Validators.required, Validators.minLength(6)]],
        });
      }
  
    onRegistrar(): void{

      let loading: Loading = this.showLoading();
      let user: User = this.registrarForm.value;
      this.navCtrl.setRoot(HomePage);
      this.authService.createUser({
        email: user.email,
        password: user.password
      });
      //console.log(this.registrarForm.value);
      this.userService.create(this.registrarForm.value);
      loading.dismiss();
      this.showAlert();
      


      this.userService.createUserSQL(this.registrarForm.value)
      .subscribe(
      data => console.log(data.mensage),
      //err=>console.log(err)
      );

      
    }

    private showLoading(): Loading {
      let loading: Loading = this.loadingCtrl.create({
        content: 'Por favor Aguarde...'
      });
      loading.present();
      return loading;
    }

    private showAlert(): void{
      this.alertCtrl.create({
        message: 'Usuario Cadastrado',
        buttons: ['OK']
      }).present();
    }

}
