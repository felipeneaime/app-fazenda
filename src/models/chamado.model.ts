export class Chamado{
    constructor(
        public titulo: string,
        public categoria: string,
        public descricao: string,
        public status: string,
        public dataAbertura: string,
        public dataFechamento: string,
        public prioridade: string,
        public usuarioAbertura: string,
        public tecnicoAtendimento: string,
        //public uid: string,
    ){}
}